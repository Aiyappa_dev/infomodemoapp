package com.infomo.admin.sdkdemoapp.jwplayer;

import com.longtailvideo.jwplayer.media.ads.AdBreak;
import com.longtailvideo.jwplayer.media.ads.AdSource;
import com.longtailvideo.jwplayer.media.ads.AdType;
import com.longtailvideo.jwplayer.media.ads.Advertising;
import com.longtailvideo.jwplayer.media.ads.ImaVMAPAdvertising;
import com.longtailvideo.jwplayer.media.ads.VMAPAdvertising;

import java.util.ArrayList;
import java.util.List;

public class SampleAds {
    /**
     * Vast Setup Example
     * {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/media/ads/Advertising.html}
     */
    //public static Advertising getVastAd() {
    public static Advertising getVastAd(String adurl) {
        List<AdBreak> adbreaklist = new ArrayList<>();
        AdBreak adbreak = new AdBreak("pre", AdSource.VAST, adurl);
        adbreak.setAdType(AdType.NONLINEAR);
        adbreaklist.add(adbreak);

//		AdRules adRules = new AdRules.Builder()
//				.frequency(1)
//				.startOn(0)
//				.startOnSeek(AdRules.RULES_START_ON_SEEK_PRE)
//				.timeBetweenAds(2)
//				.build();

        Advertising vastad = new Advertising(AdSource.VAST, adbreaklist);
//		vastad.setVpaidControls(true);
//		vastad.setAdRules(adRules);
//		vastad.setClient(AdSource.VAST);
//		vastad.setRequestTimeout(2);
//		vastad.setSkipOffset(1);
//		vastad.setAdMessage("");
//		vastad.setCueText("");
//		vastad.setSkipMessage("");
//		vastad.setSkipText("");

        return vastad;
    }

    /**
     * VAST VMAP Ad Example
     * For more info:
     * {@link - https://developer.jwplayer.com/sdk/android/docs/developer-guide/advertising/vast/#vmap-advertising}
     */
    public static VMAPAdvertising vastVMAP() {
        String samplevmap = "https://playertest.longtailvideo.com/adtags/vmap2.xml";

        VMAPAdvertising vmap = new VMAPAdvertising(AdSource.VAST, samplevmap);
        vmap.setVpaidControls(true);

        return vmap;
    }

    /**
     * IMA VMAP Ad Example
     * {@link - https://developer.jwplayer.com/sdk/android/docs/developer-guide/advertising/google-ima/#vmap-advertising}
     */
    public static ImaVMAPAdvertising imaVMAP() {
        String pre = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&cmsid=496&vid=short_onecue&correlator=";

        return new ImaVMAPAdvertising(pre);
    }

    public static VMAPAdvertising getVMAP(String client) {
        return client.equals("ima") ? imaVMAP() : vastVMAP();
    }
}
