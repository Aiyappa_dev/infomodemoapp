package com.infomo.admin.sdkdemoapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.gms.cast.framework.CastContext;
import com.infomo.admin.sdkdemoapp.jwplayer.KeepScreenOnHandler;
import com.infomo.admin.sdkdemoapp.jwplayer.SampleAds;
import com.infomo.admin.sdkdemoapp.jwplayer.SamplePlaylist;
import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.configuration.SkinConfig;
import com.longtailvideo.jwplayer.events.FullscreenEvent;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.longtailvideo.jwplayer.media.ads.Advertising;
import com.longtailvideo.jwplayer.media.ads.VMAPAdvertising;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;

import java.util.List;

public class JwPlayerActivity extends AppCompatActivity implements
        VideoPlayerEvents.OnFullscreenListener{

    private CastContext mCastContext;


    // public static String url="https://r3d.infomo.net/ads/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=preroll:0.0-0%3D567&zoneid=567&nz=1&source=&r=R0.05822725687175989&block=1&format=all&hls=&vid_width=&vid_height=&vid_min=&vid_sd=0&vid_skip=0&vid_skipafter=&vid_compad=0&c_width=&c_height=&charset=UTF-8&displayheight=1280&displaytype=Mobile&devicemodel=Che1&devicebrand=Huwaei&deviceos=Android&deviceosversion=5.1.1&carrier=airtel&device_ifa=fb100b27-4990-4822-a7f5-eb8552877bc";
    public static String url="";
    /**
     * Reference to the {@link JWPlayerView}
     */
    private JWPlayerView mPlayerView;


    /**
     * Stored instance of CoordinatorLayout
     * {@link - http://developer.android.com/reference/android/support/design/widget/CoordinatorLayout.html}
     */
    private CoordinatorLayout mCoordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jw_player);
        url=getIntent().getExtras().getString("tag_url");
        mPlayerView = findViewById(R.id.jwplayer);
        //TextView outputTextView = findViewById(R.id.output);
        //ScrollView scrollView = findViewById(R.id.scroll);
        mCoordinatorLayout = findViewById(R.id.activity_jwplayerview);

        // Print JWPlayer Version
        //outputTextView.setText(Logger.generateLogLine("JWPlayerViewExample \r\nBuild version: " + mPlayerView.getVersionCode()));

        // Handle hiding/showing of ActionBar
        mPlayerView.addOnFullscreenListener(this);

        // Keep the screen on during playback
        new KeepScreenOnHandler(mPlayerView, getWindow());

        // Instantiate the JW Player event handler class
        //new JWEventHandler(mPlayerView, outputTextView, scrollView);

        // Instantiate the JW Player Ad event handler class
        //new JWAdEventHandler(mPlayerView, outputTextView, scrollView);

        // Setup JWPlayer
        setupJWPlayer(url);

        // CastContext is lazily initialized when the CastContext.getSharedInstance() is called.
        mCastContext = CastContext.getSharedInstance(this);


    }


    /**
     * Setup JW Player
     * <p>
     * 1 - PlayerConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/PlayerConfig.Builder.html}
     * 2 - LogoConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/LogoConfig.html}
     * 3 - PlaybackRateConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/PlaybackRateConfig.html}
     * 4 - CaptionsConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/CaptionsConfig.html}
     * 5 - RelatedConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/RelatedConfig.html}
     * 6 - SharingConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/SharingConfig.html}
     * 7 - SkinConfig - {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/SkinConfig.Builder.html}
     * <p>
     * More info about our Player Configuration and other available Configurations:
     * {@link - https://developer.jwplayer.com/sdk/android/reference/com/longtailvideo/jwplayer/configuration/package-summary.html}
     */
    //private void setupJWPlayer() {
    private void setupJWPlayer(String url) {

        List<PlaylistItem> playlistItemList = SamplePlaylist.createPlaylist();
//		List<PlaylistItem> playlistItemList = SamplePlaylist.createMediaSourcePlaylist();


        // VAST Tag Example
        Advertising vastAdvertising = SampleAds.getVastAd(url);

        // VMAP Tag Example
        VMAPAdvertising vmapAdvertising = SampleAds.getVMAP("vast");

        // Skin Config
        SkinConfig skinConfig = new SkinConfig.Builder()
                .url("https://www.host.com/css/mycustomcss.css")
                .name("mycustomcss")
                .build();

        // PlayerConfig
        PlayerConfig config = new PlayerConfig.Builder()
                .playlist(playlistItemList)
                .autostart(true)
                .preload(true)
                .allowCrossProtocolRedirects(true)
                //.skinConfig(skinConfig)
                .advertising(vastAdvertising)
//				.advertising(imaAdvertising)
//				.advertising(vmapAdvertising)
                .build();

        mPlayerView.setup(config);
    }


    /*
     * In landscape mode, set to fullscreen or if the user clicks the fullscreen button
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // Set fullscreen when the device is rotated to landscape
        mPlayerView.setFullscreen(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE, false);
        super.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mPlayerView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlayerView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlayerView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPlayerView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayerView.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Exit fullscreen when the user pressed the Back button
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mPlayerView.getFullscreen()) {
                mPlayerView.setFullscreen(false, true);
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Handles JW Player going to and returning from fullscreen by hiding the ActionBar
     *
     * @param fullscreenEvent true if the player is fullscreen
     */
    @Override
    public void onFullscreen(FullscreenEvent fullscreenEvent) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (fullscreenEvent.getFullscreen()) {
                actionBar.hide();
            } else {
                actionBar.show();
            }
        }

        // When going to Fullscreen we want to set fitsSystemWindows="false"
        mCoordinatorLayout.setFitsSystemWindows(!fullscreenEvent.getFullscreen());
    }


}