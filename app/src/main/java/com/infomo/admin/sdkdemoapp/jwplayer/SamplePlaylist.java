package com.infomo.admin.sdkdemoapp.jwplayer;

import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;

import java.util.ArrayList;
import java.util.List;

public class SamplePlaylist {

    /*
     * Create a Playlist Example
     * */
    public static List<PlaylistItem> createPlaylist() {
        List<PlaylistItem> playlistItemList = new ArrayList<>();

        String[] playlist = {
                //"https://cdn.jwplayer.com/manifests/RDn7eg0o.m3u8",
               // "http://content.jwplatform.com/videos/RDn7eg0o-cIp6U8lV.mp4",
                //"https://cdn.jwplayer.com/manifests/jumBvHdL.m3u8",
                //"http://content.jwplatform.com/videos/tkM1zvBq-cIp6U8lV.mp4",
               // "https://cdn.jwplayer.com/manifests/8TbJTFy5.m3u8",
                //"https://content.jwplatform.com/videos/i3q4gcBi-cIp6U8lV.mp4",
                //"http://content.jwplatform.com/videos/iLwfYW2S-cIp6U8lV.mp4",
                "https://content.jwplatform.com/videos/8TbJTFy5-cIp6U8lV.mp4",
        };

        for (String each : playlist) {

            String[] array;

            if (each.endsWith(".m3u8")) {
                array = each.split("/manifests/");
            } else {
                array = each.split("/videos/");
            }

            PlaylistItem item = new PlaylistItem.Builder()
                    .file(each)
                    .title(array[1])
                    .build();

            playlistItemList.add(item);
        }

        return playlistItemList;
    }

}
