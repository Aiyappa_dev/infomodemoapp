package com.infomo.admin.sdkdemoapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;

/**
 * Main Activity for the IMA plugin demo. {@link ExoPlayer} objects are created by
 * {@link PlayerManager}, which this class instantiates.
 */
public final class ExoPlayerMainActivity extends AppCompatActivity {

    private PlayerView playerView;
    private PlayerManager player;
    public static String url;

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        ExoPlayerMainActivity.url = url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player_main);
        url = getIntent().getStringExtra("TAG_URL");
        playerView = findViewById(R.id.player_view);
        player = new PlayerManager(this);
    }



    @Override
    public void onResume() {
        super.onResume();
        player.init(this, playerView);
    }

    @Override
    public void onPause() {
        super.onPause();
        player.reset();
    }

    @Override
    public void onDestroy() {
        player.release();
        super.onDestroy();
    }

}
