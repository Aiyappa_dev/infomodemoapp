package com.infomo.admin.sdkdemoapp.interstitial;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;



public class BaseInterstitialActivity extends Activity {
   // @Nullable  AdReport mAdReport;
    @Nullable private CloseableLayout mCloseableLayout;
  //  @Nullable private Long mBroadcastIdentifier;

  //  protected abstract View getAdView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{

          //  Intent intent = getIntent();
     //       mBroadcastIdentifier = getBroadcastIdentifierFromIntent(intent);
          //  mAdReport = getAdReportFromIntent(intent);

            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            // getSupportActionBar().hide();
          //  View adView = getAdView();

            mCloseableLayout = new CloseableLayout(this);
            mCloseableLayout.setOnCloseListener(this::finish);

         /*   mCloseableLayout.addView(htmlAd,
                    new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
*/


           setContentView(mCloseableLayout);
        }catch (Exception r)
        {
            //LogE.printMe(r);
        }
    }

    @Override
    protected void onDestroy() {
        if (mCloseableLayout != null) {
            mCloseableLayout.removeAllViews();
        }
      //  LogE.printMe("BaseInterstitialActivity : onDestroy" );
        super.onDestroy();
    }

    /*@Nullable
    Long getBroadcastIdentifier() {
        return mBroadcastIdentifier;
    }*/

     void showInterstitialCloseButton() {
        if (mCloseableLayout != null) {
            mCloseableLayout.setCloseVisible(true);
        }
        //LogE.printMe("BaseInterstitialActivity : showInterstitialCloseButton" );
    }

     void hideInterstitialCloseButton() {
        if (mCloseableLayout != null) {
            mCloseableLayout.setCloseVisible(false);
        }
      //  LogE.printMe("BaseInterstitialActivity : hideInterstitialCloseButton" );

    }

/*
    private static Long getBroadcastIdentifierFromIntent(Intent intent) {
        if (intent.hasExtra(BROADCAST_IDENTIFIER_KEY)) {
            return intent.getLongExtra(BROADCAST_IDENTIFIER_KEY, -1L);
        }
        LogE.printMe("BaseInterstitialActivity : getBroadcastIdentifierFromIntent" );

        return null;
    }
*/
/*
    @Nullable
    private static AdReport getAdReportFromIntent(Intent intent) {
        try {
            LogE.printMe("BaseInterstitialActivity : getBroadcastIdentifierFromIntent" );

            return (AdReport) intent.getSerializableExtra(DataKeys.AD_REPORT_KEY);
        } catch (ClassCastException e) {
            return null;
        }
    }*/
}
