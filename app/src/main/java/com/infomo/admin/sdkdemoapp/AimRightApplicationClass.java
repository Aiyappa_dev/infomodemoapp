package com.infomo.admin.sdkdemoapp;

import android.app.Application;

import com.google.firebase.FirebaseApp;/*
import com.torcai.admin.androidsdk.apis.TorcaiSDK;
import com.torcai.admin.androidsdk.interfaces.TorcaiErrorListener;
import com.torcai.admin.androidsdk.utils.CommonUtils;
import com.torcai.admin.androidsdk.utils.InfomoError;
*/
import com.infomo.admin.androidsdk.apis.InfomoSDK;
import com.infomo.admin.androidsdk.interfaces.InfomoErrorListener;
import com.infomo.admin.androidsdk.utils.CommonUtils;
import com.infomo.admin.androidsdk.utils.InfomoError;

import java.util.HashMap;
import java.util.Map;


public class AimRightApplicationClass extends Application implements InfomoErrorListener {
    public static AimRightApplicationClass instance;
  //  private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseApp.initializeApp(this);
        /*this is to initialise the infomo SDK and obtain AllApis Endpoints through it. InfomoSDK.getInstance is a Singleton.*/
      InfomoSDK infomoSDK=  InfomoSDK.getInstance(this).initialize(this);
      initUserInfo("123","ajshtd", infomoSDK);
      //infomoSDK.submitEvent("","",this);
      //  refWatcher=LeakCanary.install(this);
    }
    private void initUserInfo(String fcmToken, String advertisingId, InfomoSDK infomoSDK) {
        Map<String, Object> userInfo = new HashMap();
        userInfo.put(InfomoSDK.cityName, "BLR");
        userInfo.put(InfomoSDK.countryCode, "91");
        userInfo.put(InfomoSDK.AdvertisingId,advertisingId);
        userInfo.put(InfomoSDK.countryName, "INDIA");
        userInfo.put(InfomoSDK.fcmToken, fcmToken);
        userInfo.put(InfomoSDK.sdkVersion, CommonUtils.getVersionName());
        userInfo.put(InfomoSDK.viewerPhone, "9986173 647");
        userInfo.put(InfomoSDK.zipcode, "560072");
        userInfo.put(InfomoSDK.viewerEmail, "test@1234. com");
        userInfo.put(InfomoSDK.continentName, "ASIA");
        userInfo.put(InfomoSDK.continentCode, "123");
        userInfo.put(InfomoSDK.device_email, "test@gmails. com");
        userInfo.put(InfomoSDK.height, "57");
        userInfo.put(InfomoSDK.weight, "85");
        userInfo.put(InfomoSDK.location, "BLR");
        userInfo.put(InfomoSDK.stateName, "KARNATA KA");
        userInfo.put(InfomoSDK.age,"26");
        userInfo.put(InfomoSDK.gender,"MALE");
        userInfo.put(InfomoSDK.publisherUserId,"121");
        userInfo.put(InfomoSDK.regionName,"KA");
        userInfo.put(InfomoSDK.viewerName,"DEV");
        infomoSDK.setUserInfo(userInfo);

    }

    @Override
    public void onError(InfomoError infomoError) {

    }
}
