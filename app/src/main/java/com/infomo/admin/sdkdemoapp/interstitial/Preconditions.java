package com.infomo.admin.sdkdemoapp.interstitial;

import android.util.Log;

import java.util.IllegalFormatException;

final class Preconditions {

   private static final String EMPTY_ARGUMENTS = "";

   private Preconditions() {
       // Non-instantiable.
   }

   /**
    * Ensures the truth of an expression.
    */
   public static void checkArgument(boolean expression) {
       checkArgumentInternal(expression, true, "Illegal argument.", EMPTY_ARGUMENTS);
   }

   /**
    * Ensures the truth of an expression, with an error message.
    */
   public static void checkArgument(boolean expression, String errorMessage) {
       checkArgumentInternal(expression, true, errorMessage, EMPTY_ARGUMENTS);
   }

   /**
    * Ensures the truth of an expression, with an error message that takes arguments.
    */
   public static void checkArgument(boolean expression,
           String errorMessageTemplate, Object... errorMessageArgs) {
       checkArgumentInternal(expression, true, errorMessageTemplate, errorMessageArgs);
   }



   /**
    * Ensures that an object reference is not null.
    */
   public static void checkNotNull(Object reference) {
       checkNotNullInternal(reference, true, "Object can not be null.", EMPTY_ARGUMENTS);
   }

   /**
    * Ensures that an object reference is not null, with an error message.
    */
   public static void checkNotNull(Object reference, String errorMessage) {
       checkNotNullInternal(reference, true, errorMessage, EMPTY_ARGUMENTS);
   }

   /**
    * Preconditions checks that avoid throwing and exception in release mode. These versions return
    * a boolean which the caller should check.
    */
   public final static class NoThrow {
       private static final boolean sStrictMode = false;

       /**
        * Ensures the truth of an expression.
        */
       public static boolean checkArgument(boolean expression) {
           return checkArgumentInternal(expression, sStrictMode, "Illegal argument",
                   EMPTY_ARGUMENTS);
       }


       /**
        * Ensures that an object reference is not null.
        */
       public static boolean checkNotNull(Object reference) {
           return checkNotNullInternal(reference, sStrictMode, "Object can not be null.",
                   EMPTY_ARGUMENTS);
       }

       /**
        * Ensures that an object reference is not null, with an error message.
        */
       public static void checkNotNull(Object reference, String errorMessage) {
            checkNotNullInternal(reference, sStrictMode, errorMessage, EMPTY_ARGUMENTS);
       }

   }

   private static boolean checkArgumentInternal(boolean expression, boolean allowThrow,
           String errorMessageTemplate, Object... errorMessageArgs) {
       if (expression) {
           return true;
       }
       String errorMessage = format(errorMessageTemplate, errorMessageArgs);
       if (allowThrow) {
           throw new IllegalArgumentException(errorMessage);
       }
       Log.e("SDK","errorMessage: "+errorMessage);
       return false;
   }


   @SuppressWarnings("SameParameterValue")
   private static boolean checkNotNullInternal(Object reference, boolean allowThrow,
                                               String errorMessageTemplate, Object... errorMessageArgs) {
       if (reference != null) {
           return true;
       }
       String errorMessage = format(errorMessageTemplate, errorMessageArgs);
       if (allowThrow) {
           throw new NullPointerException(errorMessage);
       }
       Log.e("SDK","errorMessage: "+errorMessage);
       return false;
   }


   private static String format(String template, Object... args) {
       template = String.valueOf(template);  // null -> "null"

       try {
           return String.format(template, args);
       } catch (IllegalFormatException exception) {
          Log.e("SDK","SDK preconditions had a format exception: " + exception.getMessage());
           return template;
       }
   }
}
