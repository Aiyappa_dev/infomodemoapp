package com.infomo.admin.sdkdemoapp;




import static com.infomo.admin.androidsdk.apis.AdSize.iaa_990x505;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.firebase.iid.FirebaseInstanceId;/*
import com.torcai.admin.androidsdk.apis.TorcaiSDK;
import com.torcai.admin.androidsdk.interfaces.TorcaiErrorListener;
import com.torcai.admin.androidsdk.utils.CommonUtils;
import com.torcai.admin.androidsdk.utils.Device_settings;
import com.torcai.admin.androidsdk.utils.InfomoConstants;
import com.torcai.admin.androidsdk.utils.InfomoError;
import com.torcai.admin.androidsdk.utils.OutsideAppZones;
import com.torcai.admin.androidsdk.view.InfomoAdGebraAdView;
*/
import com.infomo.admin.androidsdk.apis.InfomoSDK;
import com.infomo.admin.androidsdk.interfaces.InfomoErrorListener;
import com.infomo.admin.androidsdk.utils.CommonUtils;
import com.infomo.admin.androidsdk.utils.Device_settings;
import com.infomo.admin.androidsdk.utils.InfomoConstants;
import com.infomo.admin.androidsdk.utils.InfomoError;
import com.infomo.admin.androidsdk.utils.OutsideAppZones;
import com.infomo.admin.androidsdk.view.InfomoAdGebraAdView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements InfomoErrorListener {

    private Button adhesion, expand, filmStrip, flex, video, pull, reveal, slider, image, interstitial, text;
    private OutsideAppZones outsideAppZones;
    private InfomoSDK infomoSDK;
    private Button mobileInline, mobileOverlay, torcaiAd, torcaidvideoAd;
    private EditText adhesionEdit, expandEdit, filmStripEdit, flexEdit, videoEdit, pullEdit, revealEdit, sliderEdit, imageEdit, interstitialEdit, textEdit;
    private boolean resizeToFit=false;
    private EditText inlineEdit, overlayEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*initialise the views for an Activity*/
        initViews();

        /*Initialsie the sdk, This is to be called again because it's a singleton.*/
         infomoSDK=InfomoSDK.getInstance(this);

         /*Asking for Runtime permissions to access few data such as
         network type, data speed etc.*/
        askforPermissions();
        /*This is to initialise the publisher related information such debugmode, zone ids for different adFormat*/
        initPublisherInfo();
        /*This is to  register the user from the data such as Fcm Key, advertising id etc.*/
        initialiseUserData();
        /*This is to initialise the Device Information such as device type, Device width, device Height etc.*/
        initDeviceInfo();

        /*This is to sync the publisher related information from the backend and override the existing data*/
        infomoSDK.sync();

        /*this is to initialise the listeners to the specific ad types.*/
        initListeners();


       // netWorkCall.updateEventLog(InfomoConstants.KEY_END_OF_CALL, this);
      //  netWorkCall.allModuleIssues("Register User", "failure of register", "ERROR", "Exception", "test");
      //  netWorkCall.getContentsForNotification(SharedPreference.KEY_SHOWAD_ENDCALL);
       // netWorkCall.sendLogs();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
              //      netWorkCall.sendLogs();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();



        Button adgebra=findViewById(R.id.adgebra);
        adgebra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InfomoAdGebraAdView adGebraAdView=new InfomoAdGebraAdView(getApplicationContext());
                adGebraAdView.setAdSize(iaa_990x505);
                adGebraAdView.loadAd();
            }
        });
    }

    private void initDeviceInfo() {
        Device_settings dInfo = Device_settings.getSettings(this);
         Map<String, Object>  deviceInfo= new  HashMap();
        deviceInfo.put(InfomoSDK.appId, dInfo.app_id);
        deviceInfo.put(InfomoSDK.carrier, dInfo.carrier);
        deviceInfo.put(InfomoSDK.connectionType, dInfo.connection_type);
        deviceInfo.put(InfomoSDK.dataSpeed, dInfo.data_speed);
        deviceInfo.put(InfomoSDK.deviceType, dInfo.device_type);
        deviceInfo.put(InfomoSDK.didMd5, dInfo.didmd5);
        deviceInfo.put(InfomoSDK.didsha1, dInfo.didsha1);
        deviceInfo.put(InfomoSDK.displayheight, dInfo.display_height);
        deviceInfo.put(InfomoSDK.displaywidth, dInfo.display_width);
        deviceInfo.put(InfomoSDK.latitude, dInfo.user_latitude);
        deviceInfo.put(InfomoSDK.longitude, dInfo.user_longitude);
        deviceInfo.put(InfomoSDK.make, dInfo.make);
        deviceInfo.put(InfomoSDK.model, dInfo.model);
        deviceInfo.put(InfomoSDK.os, dInfo.os);

        deviceInfo.put(InfomoSDK.osv, dInfo.os_ver);
        deviceInfo.put(InfomoSDK.timeZone, dInfo.timezone);
        deviceInfo.put(InfomoSDK.userAgent, dInfo.ua);
        infomoSDK.setDeviceInfo(deviceInfo);

    }

    private void initialiseUserData() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String fcmToken = instanceIdResult.getToken();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                        String advertisingId = adInfo != null ? adInfo.getId() : null;
                        initUserInfo(fcmToken, advertisingId);
                    } catch (IOException | GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException exception) {
                        exception.printStackTrace();
                    }
                }
            };

            // call thread start for background process
            thread.start();
        });
    }

    private void initUserInfo(String fcmToken, String advertisingId) {
          Map<String, Object> userInfo = new HashMap();
        userInfo.put(InfomoSDK.cityName, "BLR");
        userInfo.put(InfomoSDK.countryCode, "91");
        userInfo.put(InfomoSDK.AdvertisingId,advertisingId);
        userInfo.put(InfomoSDK.countryName, "INDIA");
        userInfo.put(InfomoSDK.fcmToken, fcmToken);
        userInfo.put(InfomoSDK.sdkVersion, CommonUtils.getVersionName());
        userInfo.put(InfomoSDK.viewerPhone, "9986173 647");
        userInfo.put(InfomoSDK.zipcode, "560072");
        userInfo.put(InfomoSDK.viewerEmail, "test@1234. com");
        userInfo.put(InfomoSDK.continentName, "ASIA");
        userInfo.put(InfomoSDK.continentCode, "123");
        userInfo.put(InfomoSDK.device_email, "test@gmails. com");
        userInfo.put(InfomoSDK.height, "57");
        userInfo.put(InfomoSDK.weight, "85");
        userInfo.put(InfomoSDK.location, "BLR");
        userInfo.put(InfomoSDK.stateName, "KARNATA KA");
        userInfo.put(InfomoSDK.age,"26");
        userInfo.put(InfomoSDK.gender,"MALE");
        userInfo.put(InfomoSDK.publisherUserId,"121");
        userInfo.put(InfomoSDK.regionName,"KA");
        userInfo.put(InfomoSDK.viewerName,"DEV");
        infomoSDK.setUserInfo(userInfo);

    }
    /*
    * This is to
    * */

    private void initPublisherInfo() {
        Map<String, Object>  publisherVal= new HashMap();
            publisherVal.put(InfomoSDK.adDisplayMode,0);
            publisherVal.put(InfomoSDK.dailyAdTarget, 10);
            publisherVal.put(InfomoSDK.isAlarm, false);
            publisherVal.put(InfomoSDK.isEndofCall, false);
            publisherVal.put(InfomoSDK.dailyPushLimit, 10);
            publisherVal.put(InfomoSDK.isMaint, false);
            publisherVal.put(InfomoSDK.debugMode, 0);
            publisherVal.put(InfomoSDK.versionName, BuildConfig.VERSION_NAME);
            publisherVal.put(InfomoSDK.maxNotificationsPullCount, 4);
            publisherVal.put(InfomoSDK.notificationExpiry, 1000 * 60 * 60 * 6);
            publisherVal.put(InfomoSDK.templateName, InfomoConstants.KEY_ONE_INTERSTITIAL_BANNER);
            publisherVal.put(InfomoSDK.image, "557");
            publisherVal.put(InfomoSDK.text, "558");
            publisherVal.put(InfomoSDK.video_overlay, "567");
            publisherVal.put(InfomoSDK.video_inline, "566");
            publisherVal.put(InfomoSDK.video_interstitial, "589");
            publisherVal.put(InfomoSDK.interstitial, "559");
            publisherVal.put(InfomoSDK.expand, "558");
            publisherVal.put(InfomoSDK.reveal, "560");
            publisherVal.put(InfomoSDK.flex, "562");
            publisherVal.put(InfomoSDK.adhesion, "561");
            publisherVal.put(InfomoSDK.filmstrip, "564");
            publisherVal.put(InfomoSDK.slider, "563");
            publisherVal.put(InfomoSDK.pull, "565");
            publisherVal.put(InfomoSDK.isShowNotification, false);
            publisherVal.put(InfomoSDK.syncInterval, 17);
            publisherVal.put(InfomoSDK.notificationChannel, CommonUtils.getApplicationName(this));
         //   infomoSDK.setPublisherConfig(publisherVal);


    }

    private void initViews() {
        adhesion = findViewById(R.id.adhesion);
        adhesionEdit= findViewById(R.id.adhesion_edit);
        expand = findViewById(R.id.expand);
        expandEdit = findViewById(R.id.expand_edit);
        filmStrip = findViewById(R.id.film_strip);
        filmStripEdit = findViewById(R.id.film_strip_edit);
        flex = findViewById(R.id.flex);
        flexEdit = findViewById(R.id.flex_edit);
        video = findViewById(R.id.video);
        videoEdit = findViewById(R.id.video_edit);
        pull = findViewById(R.id.pull);
        pullEdit = findViewById(R.id.pull_edit);
        reveal = findViewById(R.id.reveal);
        revealEdit = findViewById(R.id.reveal_edit);
        slider = findViewById(R.id.slider);
        sliderEdit = findViewById(R.id.slider_edit);
        image = findViewById(R.id.image);
        imageEdit = findViewById(R.id.image_edit);
        interstitial = findViewById(R.id.interstitial);
        interstitialEdit = findViewById(R.id.interstitial_edit);
        text = findViewById(R.id.text);
        textEdit = findViewById(R.id.text_edit);
        mobileInline=findViewById(R.id.mobile_inline);
        mobileOverlay=findViewById(R.id.mobile_overlay);
        inlineEdit=findViewById(R.id.inline_edit);
        overlayEdit=findViewById(R.id.overlay_edit);
        torcaiAd=findViewById(R.id.rtb_ad);
        torcaidvideoAd=findViewById(R.id.torc_video_ad);
        inlineEdit.setText("https://gov.aniview.com/api/adserver/vast3/?AV_PUBLISHERID=5fe07fe623406812b82d078c&AV_CHANNELID=619b674e39138068dc0c03f8&AV_WIDTH=300&AV_HEIGHT=250");
        overlayEdit.setText("https://gov.aniview.com/api/adserver/vast3/?AV_PUBLISHERID=5fe07fe623406812b82d078c&AV_CHANNELID=619ce1fdbe50fd30e221f47d&AV_WIDTH=300&AV_HEIGHT=250");
        //cornerRadius=findViewById(R.id.image_border);
    }

    /*
    * asking for runtime permission to  access few data during device info initialisation
     * */
    private void askforPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1 ) {
            ArrayList<String> allpermissions = new ArrayList<>();
            allpermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            allpermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);

            ArrayList<String> requestpermissions = new ArrayList<String>();
            for (int i=0; i<allpermissions.size(); i++) {
                if (ContextCompat.checkSelfPermission(this, allpermissions.get(i)) == PackageManager.PERMISSION_DENIED) {
                    requestpermissions.add(allpermissions.get(i));
                }
            }
            if (requestpermissions.size() > 0) {
                String[] permissionArray = Arrays.copyOf(requestpermissions.toArray(), requestpermissions.size(), String[].class);
                ActivityCompat.requestPermissions(this, permissionArray, 11);
            }
        }

    }






    private void initListeners() {


        adhesion.setOnClickListener(v ->
                {
                    initOutsideAppZones();

                    adDisplay(adhesionEdit.getText().toString(), "banner");
                });
        expand.setOnClickListener(v ->
        {
            initOutsideAppZones();
            adDisplay(expandEdit.getText().toString(), "banner");
        });
        filmStrip.setOnClickListener(v ->
        {
            initOutsideAppZones();
            adDisplay(filmStripEdit.getText().toString(), "banner");
        });
        flex.setOnClickListener(v ->
                {
                    initOutsideAppZones();
                adDisplay(flexEdit.getText().toString(), "banner");
                });
        video.setOnClickListener(v ->
                {
                    initOutsideAppZones();
                   // adDisplay(outsideAppZones.getVideo_interstitial(), "video");
                    adDisplay(videoEdit.getText().toString(), "video");
                });
        pull.setOnClickListener(v -> {
            initOutsideAppZones();
            adDisplay(pullEdit.getText().toString(), "banner");
        });
        reveal.setOnClickListener(v -> {
            initOutsideAppZones();
            adDisplay(revealEdit.getText().toString(), "banner");
        });
        slider.setOnClickListener(v -> {
            initOutsideAppZones();
            adDisplay(sliderEdit.getText().toString(), "banner");
        });
        image.setOnClickListener(v -> {
            initOutsideAppZones();
           // adDisplay(outsideAppZones.getImage(), "banner");
            resizeToFit=true;
             adDisplay(imageEdit.getText().toString(), "banner");
        });
        interstitial.setOnClickListener(v -> {
            initOutsideAppZones();
           // adDisplay(outsideAppZones.getInterstitial(), "interstitial");
            adDisplay(interstitialEdit.getText().toString(), "interstitial");
        });
        text.setOnClickListener(v -> {
            initOutsideAppZones();
            //adDisplay(outsideAppZones.getText(), "banner");
            adDisplay(textEdit.getText().toString(), "banner");
        });
     mobileInline.setOnClickListener(v -> {
        // String url="https://r3s2.infomo.net/ads/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=preroll:0.0-0%3D157&zoneid=157&nz=1&source=&r=R0.05822725687175989&block=1&format=all&hls=&vid_width=&vid_height=&vid_min=&vid_sd=0&vid_skip=0&vid_skipafter=&vid_compad=0&c_width=&c_height=&charset=UTF-8&displaywidth={displaywidth}&displayheight={displayheight}&displaytype={displaytype}&devicemodel={devicemodel}&devicebrand={devicebrand}&deviceos={deviceos}&deviceosversion={deviceosversion}&carrier={carrier}&device_ifa={device_ifa}&device_appid={device_appid}&age=&gender=";
        //    url=inlineEdit.getText().toString();
        // loadVideo(url);
         String url="https://r3d.infomo.net/YWRz/d3d3/ZmM=.php?script=Video&em9uzwlk=1254&format=all&charset=UTF-8&displaywidth={displaywidth}&displayheight={displayheight}&displaytype={displaytype}&devicemodel={devicemodel}&devicebrand={devicebrand}&deviceos={deviceos}&deviceosversion={deviceosversion}&carrier={carrier}&device_ifa={device_ifa}&device_appid={device_appid}&age=&weight=&height=&gender=";

         loadExoPlayer(inlineEdit.getText().toString());
     });
     mobileOverlay.setOnClickListener(v -> {
            String url="https://r3s2.infomo.net/ads/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&zones=preroll:0.0-0%3D156&zoneid=156&nz=1&source=&r=R0.05822725687175989&block=1&format=all&hls=&vid_width=&vid_height=&vid_min=&vid_sd=0&vid_skip=0&vid_skipafter=&vid_compad=0&c_width=&c_height=&charset=UTF-8&displaywidth={displaywidth}&displayheight={displayheight}&displaytype={displaytype}&devicemodel={devicemodel}&devicebrand={devicebrand}&deviceos={deviceos}&deviceosversion={deviceosversion}&carrier={carrier}&device_ifa={device_ifa}&device_appid={device_appid}&age=&gender=";
       url=overlayEdit.getText().toString();
       url="https://uads.infomo.net/YWRz/d3d3/ZmM=.php?script=Video&em9uzwlk=67&format=all&charset=UTF-8&displaywidth={displaywidth}&displayheight={displayheight}&displaytype={displaytype}&devicemodel={devicemodel}&devicebrand={devicebrand}&deviceos={deviceos}&deviceosversion={deviceosversion}&carrier={carrier}&device_ifa={device_ifa}&device_appid={device_appid}&age=&weight=&height=&gender=";
         //loadVideo(url);
         loadExoPlayer(overlayEdit.getText().toString());
     });

     torcaiAd.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             adDisplay("", "torcaiad");
         }
     });

     torcaidvideoAd.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             adDisplay("", "torc_video_ad");
         }
     });

    }

    private void loadExoPlayer(String url) {
        Intent intent=new Intent(this, ExoPlayerMainActivity.class);
        intent.putExtra("TAG_URL",url);
        startActivity(intent);
    }

    private void loadVideo(String url) {
        Intent intent = new Intent(this, JwPlayerActivity.class);
        intent.putExtra("tag_url", url);
        startActivity(intent);
    }

    private void initOutsideAppZones()
    {
        try {
            outsideAppZones = InfomoSDK.getInstance(getApplicationContext()).getPublisherInfo().getOutsideAppZones();
        } catch (Exception e) {
            e.printStackTrace();
            infomoSDK.sync();
        }
    }
    private void adDisplay(String s, String banner) {
        if (s != null) {
            Intent intent = new Intent(this, MainPage.class);
            intent.putExtra("type", banner);
            intent.putExtra("zone", s);
            intent.putExtra("resizeToFit",resizeToFit);
            startActivity(intent);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        AimRightApplicationClass.instance.mustDie(this);
    }

    @Override
    public void onError(InfomoError infomoError) {

    }
}