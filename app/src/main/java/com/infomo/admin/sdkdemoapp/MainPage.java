package com.infomo.admin.sdkdemoapp;

import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;


import com.infomo.admin.androidsdk.apis.InfomoConfigException;
import com.infomo.admin.androidsdk.djax.adserver.InterstitialAdListener;
import com.infomo.admin.androidsdk.interfaces.BannerAdListeners;
import com.infomo.admin.androidsdk.interfaces.InterstitialImageAdListener;
import com.infomo.admin.androidsdk.utils.CommonUtils;
import com.infomo.admin.androidsdk.utils.LogE;
import com.infomo.admin.androidsdk.view.InfomoBannerView;
import com.infomo.admin.androidsdk.view.InfomoInterstitialAd;
import com.infomo.admin.androidsdk.view.InfomoVideoAd;


import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainPage extends AppCompatActivity implements InterstitialAdListener, InterstitialImageAdListener, BannerAdListeners {


    private InfomoBannerView infomoAdView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        String type=getIntent().getStringExtra("type");
        String zone=getIntent().getStringExtra("zone");
        boolean resizeToFit=getIntent().getBooleanExtra("resizeToFit", false);

//        ImageView imageView=findViewById(R.id.img_banner);

        if (type.equals("video")){
            InfomoVideoAd infomoVideoAd= null;
            try {

                infomoVideoAd = new InfomoVideoAd(this, this);
                infomoVideoAd.setZoneid(zone);
                infomoVideoAd.setKeywords("Sports,Politics,Art,Movies,Books");
                infomoVideoAd.setProfileInfo("age=25&gender=Male&height=165&weight=60");
                infomoVideoAd.LoadAd();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (type.equals("interstitial"))
        {
            InfomoInterstitialAd infomoAdView= null;
            try {
                infomoAdView = new InfomoInterstitialAd(this);
                infomoAdView.setZoneid(zone);
                infomoAdView.setProfileInfo("age=25&gender=Male&height=165&weight=60");
                infomoAdView.setKeywords("Sports,Politics,Art,Movies,Books");
                infomoAdView.LoadAd();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }/*
        else if (type.equals("torcaiad"))
        {

            RelativeLayout news_detail_banner_container=findViewById(R.id.banner);
            try {
                Torcai infomoRTB=new Torcai(this);
                infomoRTB.setDm(CommonUtils.getApplicationName(this));
                infomoRTB.setAdid("Test_Mweb");
                infomoRTB.setIp("103.5.160.196");
                infomoRTB.setDevice("android");
                infomoRTB.setDeviceId("62c7ju33uj5l7as8");
                infomoRTB.setAppId(BuildConfig.APPLICATION_ID);
                infomoRTB.setAppName(CommonUtils.getApplicationName(this));
                infomoRTB.setAppBundle(BuildConfig.APPLICATION_ID);
                infomoRTB.setAppStoreUrl("https://play.google.com/store/apps/details?id=net.one97.paytm");
                infomoRTB.setAppUA(System.getProperty("http.agent"));
                //infomoRTB.setPurl("www.momspresso.com/category/");
                infomoRTB.loadAd();
                RelativeLayout.LayoutParams param1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                infomoRTB.setLayoutParams(param1);
                news_detail_banner_container.removeAllViews();
                news_detail_banner_container.bringToFront();
                news_detail_banner_container.addView(infomoRTB);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (type.equalsIgnoreCase("torc_video_ad"))
        {
            TorcaiVideo infomoRTB;
            try {
                infomoRTB = new TorcaiVideo(this, this);
                infomoRTB.setAdid("Test_Video");
                infomoRTB.setDm(CommonUtils.getApplicationName(this));
                infomoRTB.setIp("103.5.160.196");
                infomoRTB.setDevice("android");
                infomoRTB.setDeviceId("62c7ju33uj5l7as8");
                infomoRTB.setAppId(BuildConfig.APPLICATION_ID);
                infomoRTB.setAppName(CommonUtils.getApplicationName(this));
                infomoRTB.setAppBundle(BuildConfig.APPLICATION_ID);
                infomoRTB.setAppStoreUrl("https://play.google.com/store/apps/details?id=net.one97.paytm");
                infomoRTB.setAppUA(System.getProperty("http.agent"));

                infomoRTB.LoadAd();
            } catch (InfomoConfigException e) {
                e.printStackTrace();
            }
        }*/
        else {

            RelativeLayout news_detail_banner_container=findViewById(R.id.banner);

            try {
                 infomoAdView1 = new InfomoBannerView(this);
                infomoAdView1.setResizeToFit(resizeToFit);
                infomoAdView1.setMargin(0);
                infomoAdView1.setPadding(0);
                infomoAdView1.setCornerRadius(100);
                infomoAdView1.setZoneid(zone);
                infomoAdView1.setProfileInfo("age=25&gender=Male&height=165&weight=60");
                infomoAdView1.setKeywords("Sports,Politics,Art,Movies,Books");
                infomoAdView1.LoadAd();
            RelativeLayout.LayoutParams param1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                infomoAdView1.setLayoutParams(param1);
                news_detail_banner_container.removeAllViews();
            news_detail_banner_container.bringToFront();
                news_detail_banner_container.addView(infomoAdView1);


            } catch (Exception e) {
                e.printStackTrace();
            }

            /*try {
                String html = "<a href=\"https://publisher.torcai.com/clickfacade/clicks?rt=2021-11-23+21%3A10%3A12.237&amp;xid=torcai_ion&amp;cid=194&amp;cname=Test_BankMadiri_Mweb&amp;crid=208&amp;crname=BankMandiri_TestCreatives_Mweb&amp;sid=196&amp;sname=Test_BM_Mweb&amp;ioid=88&amp;ioname=BankMandiri_TestIO&amp;tid=166&amp;tname=Infomo&amp;iid=bid1&amp;size=250x250&amp;os=android&amp;br=android&amp;ct=&amp;state=&amp;cntry=japan&amp;dt=mobile&amp;client=torcai&amp;event=click&amp;recordId=b081de10-bc2d-472f-a9d2-45ad647255ca_1&amp;dmn=Infomo+Demo+App&amp;uid=&amp;dspuid=&amp;appBundle=&amp;crdid=df_503&amp;appflag=0&amp;rUrl=https%253A%252F%252Fbankmandiri.co.id%252F\" target=\"_blank\"><img src=\"https://cdn.torcai.com/images/ion-ads/images/165/250x250Indo.png\" width=\"100%25 %21important\" height=\"250\" /><iframe id=\"ncImpression\" style=\"height: 0px; width: 0px; display: none; border: 0px;\" src=\"https://publisher.torcai.com/notification/impression?rt=2021-11-23+21%3A10%3A12.237&amp;xid=torcai_ion&amp;cid=194&amp;cname=Test_BankMadiri_Mweb&amp;crid=208&amp;crname=BankMandiri_TestCreatives_Mweb&amp;sid=196&amp;sname=Test_BM_Mweb&amp;ioid=88&amp;ioname=BankMandiri_TestIO&amp;tid=166&amp;tname=Infomo&amp;iid=bid1&amp;size=250x250&amp;os=android&amp;br=android&amp;ct=&amp;state=&amp;cntry=japan&amp;dt=mobile&amp;client=torcai&amp;event=imp&amp;recordId=b081de10-bc2d-472f-a9d2-45ad647255ca_1&amp;dmn=Infomo+Demo+App&amp;uid=&amp;dspuid=&amp;appBundle=&amp;crdid=df_503&amp;appflag=0&amp;ai=752e15be-f827-4140-9d7c-9188f1cc7790&amp;impid=1&amp;au=Test_Mweb&amp;seatid=116&amp;wp=1.10000002&amp;cur=USD\" width=\"0\" height=\"0\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\"></iframe></a>";

                String htmls = URLDecoder.decode(html, "UTF-8");
           //  String htmlCode= String.valueOf(Html.fromHtml(html , Html.FROM_HTML_MODE_LEGACY));
                WebView webView = findViewById(R.id.banner);
                webView.getSettings().setJavaScriptEnabled(true);


                webView.loadDataWithBaseURL("", htmls, "text/html", "utf-8", "");

                *//*if(html.contains("iframe")){
                    Matcher matcher = Pattern.compile("src=\"([^\"]+)\"").matcher(html);
                    matcher.find();
                    String src = matcher.group(1);
                    html=src;

                    try {
                       // URL myURL = new URL(src);
                        htmlAd.getSettings().setAllowFileAccess(true);
                        htmlAd.loadUrl(src);
                       // htmlAd.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + src, "text/html", "UTF-8", null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {

                    htmlAd.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + html, "text/html", "UTF-8", null);
                }*//*



                //  this.ad_container_params = new FrameLayout.LayoutParams(-2, -2);
                //htmlAd.setLayoutParams(this.ad_container_params);
               //  this.ad_container.removeAllViews();
               // this.ad_container.addView(htmlAd);
            } catch (Exception var5) {
                var5.printStackTrace();
            }*/
        }

    }


    @Override
    public void AdLoaded() {
        LogE.printMe("Video Ad Loaded");
    }

    @Override
    public void AdFailed() {
        LogE.printMe("Video Ad Failed");
    }

    @Override
    public void Adclosed() {
        LogE.printMe("Video Ad Closed");
    }

    @Override
    public void Adclicked() {
        LogE.printMe("Video Ad Clicked");
    }

    @Override
    public void Adshown() {
        LogE.printMe("Video Ad Shown");
    }

    @Override
    public void onInterstitialAdLoaded() {
        LogE.printMe("ad loaded");
    }

    @Override
    public void onInterstitialAdFailed() {
        LogE.printMe("Interstitial ad failed");
    }

    @Override
    public void onInterstitialAdShown() {
        LogE.printMe("Interstitial ad shown");
    }

    @Override
    public void onInterstitialAdClicked() {
        LogE.printMe("Interstitial ad clicked");
    }

    @Override
    public void onInterstitialAdDismissed() {
        LogE.printMe("Interstitial ad dismissed");
    }

    @Override
    public void onBannerAdLoaded(Object adObject) {
        /*InfomoBannerView infomoAdView=(InfomoBannerView) adObject;
        if (infomoAdView1.getZoneid().equals( infomoAdView.getZoneid()))
        {
            LogE.printMe("ad Loaded");
        }*/

    }

    @Override
    public void onBannerAdFailed(Object ad) {
        LogE.printMe("ad Failed");
    }

    @Override
    public void onBannerAdClicked(Object ad) {
        LogE.printMe("ad clicked");
    }
}