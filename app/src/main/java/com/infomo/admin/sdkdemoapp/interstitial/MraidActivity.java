package com.infomo.admin.sdkdemoapp.interstitial;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;


import java.io.Serializable;

public class MraidActivity extends BaseInterstitialActivity {
  /*  @Nullable
    private MraidController mNativeImgLVMraidController;
    @SuppressWarnings("unused")
    @Nullable
    private MraidWebViewDebugListener mDebugListener;
    @Nullable
    private ExternalViewabilitySessionManager mNativeImgLVExternalViewabilitySessionManager;
*/
    /*public static void preRenderHtml(@NonNull final Interstitial mraidNativeImgLVInterstitial,
                                     @NonNull final Context context,
                                     @NonNull final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener,
                                     @Nullable final String htmlData,
                                     @Nullable final String adtype,
                                     @NonNull final Long broadcastIdentifier,
                                     @Nullable final AdReport nativeImgLVAdReport) {
        Preconditions.checkNotNull(mraidNativeImgLVInterstitial);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(customEventInterstitialListener);
        Preconditions.checkNotNull(broadcastIdentifier);

        preRenderHtml(mraidNativeImgLVInterstitial, customEventInterstitialListener, htmlData,adtype,
                new MraidBridge.MraidWebViewNativeImgLV(context), broadcastIdentifier,
                new MraidController(context, nativeImgLVAdReport, PlacementType.INTERSTITIAL));
    }
*/
  /*  @VisibleForTesting
   private static void preRenderHtml(@NonNull final Interstitial mraidNativeImgLVInterstitial,
                              @NonNull final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener,
                              @Nullable final String htmlData,
                              @Nullable final String adtype,
                              @NonNull final BaseWebView mraidWebView,
                              @NonNull final Long broadcastIdentifier,
                              @NonNull final MraidController nativeImgLVMraidController) {
        MInterstitial_Log.log(LOAD_ATTEMPTED );
        Preconditions.checkNotNull(mraidNativeImgLVInterstitial);
        Preconditions.checkNotNull(customEventInterstitialListener);
        Preconditions.checkNotNull(mraidWebView);
        Preconditions.checkNotNull(broadcastIdentifier);
        Preconditions.checkNotNull(nativeImgLVMraidController);

        mraidWebView.enableJavascriptCaching();
        final Context context = mraidWebView.getContext();
        mraidWebView.setInitialScale(1);
        mraidWebView.getSettings().setLoadWithOverviewMode(true);
        mraidWebView.getSettings().setUseWideViewPort(true);
        mraidWebView.setWebViewClient(new MraidWebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (LPUB_FAIL_LOAD.equals(url)) {
                    MInterstitial_Log.log(LOAD_FAILED, ErrorCodes.VIDEO_CACHE_ERROR.getIntCode(),
                            ErrorCodes.VIDEO_CACHE_ERROR);

                    customEventInterstitialListener.onInterstitialFailed(
                            ErrorCodes.MRAID_LOAD_ERROR);
                }
                return true;
            }

            @Override
            public void onPageFinished(final WebView view, final String url) {
                MInterstitial_Log.log(LOAD_SUCCESS);
                customEventInterstitialListener.onInterstitialLoaded();
                nativeImgLVMraidController.onPreloadFinished(mraidWebView);
            }

            @Override
            public void onReceivedError(final WebView view, final int errorCode,
                                        final String description,
                                        final String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                MInterstitial_Log.log(LOAD_FAILED, ErrorCodes.VIDEO_CACHE_ERROR.getIntCode(),
                        ErrorCodes.VIDEO_CACHE_ERROR);
                customEventInterstitialListener.onInterstitialFailed(
                        ErrorCodes.MRAID_LOAD_ERROR);
            }
        });


        final ExternalViewabilitySessionManager nativeImgLVExternalViewabilitySessionManager =
                new ExternalViewabilitySessionManager(context);
        nativeImgLVExternalViewabilitySessionManager.createDisplaySession(context, mraidWebView, true);

      //  mraidWebView.loadDataWithBaseURL(Networking.getBaseUrlScheme() + "://" + Constants.HOST + "/",
        //        htmlData, "text/html", "UTF-8", null);
        //Google Ads passback start
        if (adtype != null) {
            if(adtype.equalsIgnoreCase("passback interstitialad")) {
                if (htmlData != null) {
               mraidWebView.loadUrl(htmlData);
               }
            }
            else {
                if (htmlData != null) {
                mraidWebView.loadDataWithBaseURL(Constants.HTTP + "://" + Constants.HOST + "/",
                        htmlData, "text/html", "UTF-8", null);}
            }
        }
        //Google Ads passback End

        WebViewCacheService.storeWebViewConfig(broadcastIdentifier, mraidNativeImgLVInterstitial,
                mraidWebView, nativeImgLVExternalViewabilitySessionManager, nativeImgLVMraidController);
    }
*/


  /*  public static void start(@NonNull final Context context,
                             @Nullable final AdReport adreport,
                             @Nullable final String htmlData,
                             final long broadcastIdentifier,
                             @Nullable final CreativeOrientation orientation) {
        MInterstitial_Log.log(SHOW_ATTEMPTED);
        final Intent intent = createIntent(context, adreport, htmlData, broadcastIdentifier,
                orientation);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException exception) {
            MInterstitial_Log.log(SHOW_FAILED, ErrorCodes.INTERNAL_ERROR.getIntCode(),
                    ErrorCodes.INTERNAL_ERROR);
            Log.d("MraidInterstitial", "MraidActivity.class not found. Did you declare MInterstitial_MraidActivity in your manifest?");
        }
    }
*/
   /* @VisibleForTesting
    private static Intent createIntent(@NonNull final Context context,
                                       @Nullable final AdReport nativeImgLVAdReport,
                                       @Nullable final String htmlData,
                                       final long broadcastIdentifier,
                                       @Nullable final CreativeOrientation orientation) {
        Intent intent = new Intent(context, MraidActivity.class);
        intent.putExtra(HTML_RESPONSE_BODY_KEY, htmlData);
        intent.putExtra(BROADCAST_IDENTIFIER_KEY, broadcastIdentifier);
        intent.putExtra(AD_REPORT_KEY, nativeImgLVAdReport);
        intent.putExtra(CREATIVE_ORIENTATION_KEY, orientation);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
*//*
    @Override
    public View getAdView() {
        String htmlData = getIntent().getStringExtra(HTML_RESPONSE_BODY_KEY);
        if (htmlData == null) {
            Log.d("SDK","MInterstitial_MraidActivity received a null HTML body. Finishing the activity.");
            finish();
            return new View(this);
        }

        final Long broadcastIdentifier = getBroadcastIdentifier();
        WebViewCacheService.Config config = null;
        if (broadcastIdentifier != null) {
            config = WebViewCacheService.popWebViewConfig(broadcastIdentifier);
        }

        if (config != null && config.getController() != null) {
            mNativeImgLVMraidController = config.getController();
        } else {
            mNativeImgLVMraidController = new MraidController(
                    this, mAdReport, PlacementType.INTERSTITIAL);
        }

        mNativeImgLVMraidController.setDebugListener(mDebugListener);
        mNativeImgLVMraidController.setMraidListener(new MraidController.MraidListener() {
            @Override
            public void onLoaded() {
                // This is only done for the interstitial. Banners have a different mechanism
                // for tracking third party impressions.
                mNativeImgLVMraidController.loadJavascript(WEB_VIEW_DID_APPEAR.getJavascript());
            }

            @Override
            public void onFailedToLoad() {
                Log.d("SDK", "MInterstitial_MraidActivity failed to load. Finishing the activity");
                if (getBroadcastIdentifier() != null) {
                    BaseBroadcastReceiver.broadcastAction(MraidActivity.this, getBroadcastIdentifier(),
                            ACTION_INTERSTITIAL_FAIL);
                }
                finish();
            }

            public void onClose() {
                MInterstitial_Log.log(WILL_DISAPPEAR);
                mNativeImgLVMraidController.loadJavascript(WEB_VIEW_DID_CLOSE.getJavascript());
                finish();
            }

            @Override
            public void onExpand() {
                // No-op. The interstitial is always expanded.
            }

            @Override
            public void onOpen() {
                MInterstitial_Log.log(DID_APPEAR);
                if (getBroadcastIdentifier()!= null) {
                    BaseBroadcastReceiver.broadcastAction(MraidActivity.this, getBroadcastIdentifier(),
                            ACTION_INTERSTITIAL_CLICK);
                }
                finish();
            }
        });

        // Needed because the Activity provides the close button, not the controller. This
        // gets called if the creative calls mraid.useCustomClose.
        mNativeImgLVMraidController.setUseCustomCloseListener(useCustomClose -> {
            if (useCustomClose) {
                hideInterstitialCloseButton();
            } else {
                showInterstitialCloseButton();
            }
        });

        if (config != null) {
            mNativeImgLVExternalViewabilitySessionManager = config.getViewabilityManager();
        } else {
            mNativeImgLVMraidController.fillContent(htmlData,
                    (webView, viewabilityManager) -> {
                        if (viewabilityManager != null) {
                            mNativeImgLVExternalViewabilitySessionManager = viewabilityManager;
                        } else {
                            mNativeImgLVExternalViewabilitySessionManager = new ExternalViewabilitySessionManager(MraidActivity.this);
                            mNativeImgLVExternalViewabilitySessionManager.createDisplaySession(MraidActivity.this, webView, true);
                        }
                    });

        }

        return mNativeImgLVMraidController.getAdContainer();
    }*/

    /*@Override
    protected View getAdView() {
        return null;
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   MInterstitial_Log.log(SHOW_SUCCESS);

       /* final Serializable orientationExtra = getIntent().getSerializableExtra(
                CREATIVE_ORIENTATION_KEY);
        CreativeOrientation requestedOrientation = CreativeOrientation.DEVICE;
        if (orientationExtra instanceof CreativeOrientation) {
            requestedOrientation = (CreativeOrientation) orientationExtra;
        }
        DeviceUtils.lockOrientation(this, requestedOrientation);

        if (mNativeImgLVExternalViewabilitySessionManager != null) {
            mNativeImgLVExternalViewabilitySessionManager.startDeferredDisplaySession(this);
        }
        if (getBroadcastIdentifier()!= null) {
            BaseBroadcastReceiver.broadcastAction(this, getBroadcastIdentifier(), ACTION_INTERSTITIAL_SHOW);
        }
*/
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        /*if (mNativeImgLVMraidController != null) {
            mNativeImgLVMraidController.onShow(this);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInterstitialCloseButton();
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
